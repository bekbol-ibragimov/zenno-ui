import {LocationOfAnswersEnum} from "./location-of-answers.enum";

export class QuestionAddDto {
  name?: string;
  createDate: Date = new Date();
  containsCyrillic: boolean = false;
  containsLatin: boolean = false;
  containsNumbers: boolean = false;
  containsSpecialCharacters: boolean  = false;
  caseSensitivity: boolean = false;
  locationOfAnswers: LocationOfAnswersEnum = LocationOfAnswersEnum.Absent;
  file?: File;

}
