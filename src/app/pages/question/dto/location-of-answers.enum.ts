export enum LocationOfAnswersEnum {
  Absent = 'Absent',
  InFilenames = 'InFilenames',
  InSeparateFile = 'InSeparateFile'
}
