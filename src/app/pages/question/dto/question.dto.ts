export class QuestionDto {
  name?: string;
  createDate?: string;
  containsCyrillic?: boolean;
  containsLatin?: boolean;
  containsNumbers?: boolean;
  containsSpecialCharacters?: boolean;
  caseSensitivity?: boolean;
  locationOfAnswers?: string;
  file?: string;

}
