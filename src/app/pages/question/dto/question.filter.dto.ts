import {PaginationQueryDto} from "../../../core/dto/pagination-query.dto";
import {SortDirection} from "../../../core/enums/sort-direction.enum";

export class QuestionFilterDto extends PaginationQueryDto<QuestionOrderBy>{
  constructor(){
    super();
    this.sortField = QuestionOrderBy.id
    this.sortDirection = SortDirection.Descending;
  }
  SearchString?: string;
}

export enum QuestionOrderBy
{
  id = 'Id',
  name = 'Name',
}
