import { NgModule } from '@angular/core';
import { QuestionRoutingModule } from './question-routing.module';
import { QuestionComponent } from './question.component';
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {QuestionService} from "./question.service";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTableModule} from "ng-zorro-antd/table";
import {CommonModule} from "@angular/common";


@NgModule({
  imports: [
    CommonModule,
    // BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    QuestionRoutingModule,
    NzGridModule,
    NzInputModule,
    NzButtonModule,
    NzDatePickerModule,
    NzCheckboxModule,
    NzFormModule,
    NzSelectModule,
    NzTableModule
  ],
  declarations: [QuestionComponent],
  exports: [QuestionComponent],
  providers: [QuestionService]
})
export class QuestionModule { }
