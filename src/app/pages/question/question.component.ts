import { Component, OnInit } from '@angular/core';
import {QuestionService} from "./question.service";
import {QuestionFilterDto} from "./dto/question.filter.dto";
import {QuestionAddDto} from "./dto/question-add.dto";
import {PaginationResponseDto} from "../../core/dto/pagination-response.dto";
import {QuestionDto} from "./dto/question.dto";
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
  selector: 'app-welcome',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  addDto: QuestionAddDto = new QuestionAddDto();
  pagination: PaginationResponseDto<QuestionDto> = new PaginationResponseDto<QuestionDto>();
  questions: QuestionDto[] = [];
  constructor(private questionService: QuestionService, private message: NzMessageService) { }

  ngOnInit() {
    this.addDto = new QuestionAddDto();
    let filter = new QuestionFilterDto();
    this.questionService.getList(filter).subscribe(x => {
      this.pagination = x;
      this.questions = x.items as QuestionDto[];
    });

  }

  onSubmit(){
    console.log(this.addDto);
    this.questionService.add(this.addDto).subscribe(x => {
      this.ngOnInit();
      this.message.success("Данные успешно дабавлены.");
    });
  }

  onFileChange(event: any) {
    console.log('event', event)
    if (event.target.files.length > 0) {
      this.addDto.file = event.target.files[0];
    }

  }

}
