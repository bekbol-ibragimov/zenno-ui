import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {QuestionFilterDto} from "./dto/question.filter.dto";
import {BaseService} from "../../core/services/base.service";
import {PaginationResponseDto} from "../../core/dto/pagination-response.dto";
import {QuestionDto} from "./dto/question.dto";
import {QuestionAddDto} from "./dto/question-add.dto";

@Injectable({
  providedIn: 'root'
})
export class QuestionService extends BaseService {

  private baseUrl = '/api/Questions/';

  constructor(protected http: HttpClient) {
    super(http);
  }

  getList(filter: QuestionFilterDto): Observable<PaginationResponseDto<QuestionDto>> {
    return this.baseGet<PaginationResponseDto<QuestionDto>>(this.baseUrl, filter);
  }

  add(dto: QuestionAddDto): Observable<QuestionDto>{
    let formData = this.toFormData(dto);
    return this.http.post<QuestionDto>( this.baseUrl+ "/Add", formData);
  }



}
