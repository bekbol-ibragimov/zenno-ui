import {Injectable} from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HTTP_INTERCEPTORS,
    HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd/message';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {


    constructor(private router: Router, private message: NzMessageService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      return next.handle(req).pipe(
        catchError((err: HttpErrorResponse) => {
          console.log('err', err)
          if (err.status === 0) {
              this.message.error('Нет соединения');
            return throwError("Нет соединения");
          } else {
              // переделать под бек
            const errors = err.error.errors;
            if(Array.isArray(errors)){
              errors.forEach((error: any) => {
                this.message.error(error.message);
              })
            } else {
              for (const key in errors) {
                this.message.error(errors[key]);
              }
            }

              return throwError(errors);
          }
        })
      );
    }
}

export const ErrorInterceptorProviders = [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
];
