import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
    constructor() {
    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


        return next.handle(req)
            .pipe(
            map(resp => {
                if (resp instanceof HttpResponse && resp.body.succeeded) {
                    resp = resp.clone<any>({ body: resp.body.data});
                    return resp;
                }
                else{
                    return resp;
                }
            })
            )
            ;
    }
}

export const DefaultInterceptorProviders =
    {provide: HTTP_INTERCEPTORS, useClass: DefaultInterceptor, multi: true}
;
