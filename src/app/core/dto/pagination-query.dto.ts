import {BaseSortFilter} from './base-sort.filter';

export class PaginationQueryDto<TOrderBy> extends BaseSortFilter<TOrderBy>{
  pageNumber = 1;
  pageSize = 50;
}
