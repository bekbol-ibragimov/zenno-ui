export class PaginationResponseDto<T> {
  total?: number;
  items?: T[];
}
