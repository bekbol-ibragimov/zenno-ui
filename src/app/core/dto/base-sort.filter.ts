import {SortDirection} from '../enums/sort-direction.enum';

export class BaseSortFilter<TOrderBy> {
  sortField?: TOrderBy;
  sortDirection: SortDirection = SortDirection.Ascending;
}
