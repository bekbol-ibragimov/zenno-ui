import { Injectable } from '@angular/core';
import * as moment from 'moment';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(protected http: HttpClient) {
  }

  protected baseGet<T>(uri: string, params: any): Observable<T> {
    const pars = this.getUrlParams(params);
    return this.http.get<T>( uri, {params: pars});
  }

  protected basePost<T>(uri: string, body: any): Observable<T> {
    body = this.normalBody(body);
    return this.http.post<T>( uri, body);
  }

  protected basePut<T>(uri: string, body: any): Observable<T> {
    body = this.normalBody(body);
    return this.http.put<T>( uri, body);
  }

  protected baseDelete(uri: string, body: any): Observable<any> {
    body = this.normalBody(body);
    return this.http.delete( uri, body);
  }

  public baseGetFile(uri: string, params: any) {
    const pars = this.getUrlParams(params);
    this.http.get( uri, {params: pars, responseType: 'blob', observe: 'response'})
      .subscribe((response: HttpResponse<Blob>) => {
        this.downloadFile(response);
      });
  }
  public basePostFile(uri: string, body: any) {
    this.http.post( uri, body, { responseType: 'blob', observe: 'response'})
      .subscribe((response: HttpResponse<Blob>) => {
        this.downloadFile(response);
      });
  }
  public downloadFile(response: HttpResponse<Blob>) {
    let disposition = response.headers.get('content-disposition');
    const url= window.URL.createObjectURL(response.body);
    const link = document.createElement('a');
    if(disposition != null){
      let fileName = decodeURI(disposition.substring(disposition.indexOf("filename*=UTF-8''") + 17));
      link.download = fileName;
    }
    link.href = url;
    link.click();

  }

  protected getUrlParams(body: any): HttpParams {
    let params = new HttpParams();
    for (const key in body) {
      if (!body.hasOwnProperty(key)) {
        continue;
      }
      else if (body[key] instanceof Date) {
        body[key] = this.formatDate(body[key]);
      }

      if (body[key] !== undefined) {
        params = params.append(key, body[key]);
      }
    }
    return params;
  }

  protected normalBody(body: any): any {
    console.log('body', body);
    if (!body) {
      body = {};
    }
    for (const key in body) {
      if (!body.hasOwnProperty(key)) {
        continue;
      }
      if (body[key] instanceof Date) {
        body[key] = this.formatDate(body[key]);
      }
    }
    return body;
  }

  protected toFormData(body: any): FormData {
    console.log('body', body);
    let formData: any = new FormData();
    if (!body) {
      body = {};
    }
    for (const key in body) {
      if (!body.hasOwnProperty(key)) {
        continue;
      }
      if (body[key] instanceof Date) {
        body[key] = this.formatDate(body[key]);
      }
      formData.append(key, body[key]);
    }
    console.log('formData', formData);
    return formData;
  }

  formatDate(date: Date) {
    return moment(date).format('YYYY-MM-DD[T]HH:mm:ss');
  }
}
